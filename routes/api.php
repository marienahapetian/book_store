<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//

//header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
//header('Access-Control-Allow-Headers: Content-Type, Accept, Authorization, X-Requested-With, Application');

Route::middleware('auth:api')->group( function (){
    Route::post('/logout','api\AuthController@logout');

});
Route::post('/login','api\AuthController@login');

Route::post('/register','api\AuthController@register');


Route::get('book/search','api\BookController@search')->name('book.search');

Route::apiResource('book','api\BookController');
