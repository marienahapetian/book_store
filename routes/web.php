<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/books', 'BookController@index')->middleware('auth')->name('books');
Route::get('/book/add', 'BookController@create')->middleware('auth')->name('book.web.add');
Route::post('/book/create', 'BookController@store')->middleware('auth')->name('book.web.store');
Route::get('/book/{id}', 'BookController@show')->middleware('auth')->where('id', '[0-9]+')->name('book.web.single');
Route::get('/book/{id}/edit', 'BookController@edit')->middleware('auth')->name('book.web.edit');
Route::post('/book/{id}/update', 'BookController@update')->middleware('auth')->name('book.web.update');
Route::get('/book/{id}/delete', 'BookController@delete')->middleware('auth')->name('book.web.delete');


Route::get('/clear-cache', function() {
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
});
