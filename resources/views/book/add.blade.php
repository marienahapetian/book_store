@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('Add Book')}}</div>

                    <div class="card-body">
                        @if ( session('status') )
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div id="app">
                            <form method="post" action="{{ url('book/create') }}" enctype="multipart/form-data">
                                @csrf
                                {{--form inputs--}}
                                <div class="row">
                                    <div class="form-group col-md-8 offset-md-2 ">
                                        <label for="title">{{__('Title')}}:</label>
                                        <input type="text" class="form-control title-input" name="title">

                                        {!! $errors->first('title', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-8 offset-md-2 ">
                                        <label for="price">{{__('Price')}}:</label>
                                        <input type="number" class="form-control title-input" name="price">

                                        {!! $errors->first('price', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-8 offset-md-2 ">
                                        <label for="author">{{__('Author')}}:</label>
                                        <input type="text" class="form-control author-input" name="author">

                                        {!! $errors->first('author', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-8 offset-md-2 ">
                                        <label for="publisher">{{__('Publisher')}}:</label>
                                        <input type="text" class="form-control publisher-input" name="publisher">

                                        {!! $errors->first('publisher', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-8 offset-md-2 ">
                                        <label for="isbn">{{__('ISBN')}}:</label>
                                        <input type="text" class="form-control isbn-input" name="isbn">

                                        {!! $errors->first('isbn', '<p class="error-block">:message</p>') !!}
                                    </div>
                                    <div class="form-group col-md-8 offset-md-2  ">
                                        <label for="published">{{__('Published')}}:</label>
                                        <input type="date" class="form-control published-input" name="published">
                                        {!! $errors->first('published', '<p class="error-block">:message</p>') !!}
                                    </div>

                                    <div class="form-group col-md-8  offset-md-2 ">
                                        <button type="submit" class="btn btn-success">{{__('Create')}}</button>
                                        <a href="{{ url()->previous() }}" class="btn btn-cancel">{{__('Cancel')}}</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
