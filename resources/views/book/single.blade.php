@extends('layouts.app')

@section('content')
    <div id="app" class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <div class="col-md-12">
                            <div class="row">
                                <h5 class="page-title">{{$book->title}}</h5>
                            </div>

                            <div class="row">
                                <strong>{{__('Author')}}:</strong> {{$book->author}}
                            </div>

                            <div class="row">
                                <strong>{{__('Price')}}:</strong> {{$book->price}}$
                            </div>

                            <div class="row">
                                <strong>{{__('Publisher')}}:</strong> {{$book->publisher}}
                            </div>

                            <div class="row">
                                <strong>{{__('ISBN')}}:</strong> {{$book->isbn}}
                            </div>

                            <div class="row">
                                <strong>{{__('Published')}}:</strong> {{$book->published}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
