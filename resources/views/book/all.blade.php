@extends('layouts.app')

@section('content')
    <div id="app" class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{__('Books')}}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        @foreach($books as $book)
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="{{$book->thumbnail}}" style="max-width: 100%"/>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <h5 class="page-title">{{$book->title}}</h5>
                                    </div>

                                    <div class="row">
                                        <strong>{{__('Author')}}:</strong> {{$book->author}}
                                    </div>

                                    <div class="row">
                                        <strong>{{__('Price')}}:</strong> {{$book->price}}$
                                    </div>

                                    <div class="row">
                                        <strong>{{__('Publisher')}}:</strong> {{$book->publisher}}
                                    </div>

                                    <div class="row">
                                        <strong>{{__('ISBN')}}:</strong> {{$book->isbn}}
                                    </div>

                                    <div class="row">
                                        <strong>{{__('Published')}}:</strong> {{$book->published}}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        {{ $books->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
