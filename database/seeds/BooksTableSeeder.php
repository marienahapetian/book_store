<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = array(
            array(
                'title' => 'One Indian Girl',
                'user_id'=>'1',
                'author' => 'Chetan Bhagat',
                'isbn' => '978812914216',
                'publisher' => 'Rupa Publications',
                'published' => Carbon::parse('2016-10-01'),
                'price' => '10.00',
                'thumbnail' => 'https://images-na.ssl-images-amazon.com/images/I/51m34YKM-%2BL._SX314_BO1,204,203,200_.jpg'
            ),
            array(
                'title' => 'The Notebook',
                'user_id'=>'1',
                'author' => 'Nicholas Sparks',
                'isbn' => '978075153815',
                'publisher' => 'TIME WARNER PAPERBAC',
                'published' => Carbon::parse('2006-12-01'),
                'price' => '7.70',
                'thumbnail' => 'https://www.hachettebookgroup.com/wp-content/uploads/2017/06/9780446676090.jpg'
            ),
            array(
                'title' => 'Two Dark Reigns (Three Dark Crowns)',
                'author' => 'Kendare Blake',
                'isbn' => '978006268645',
                'publisher' => 'HarperTeen',
                'published' => Carbon::parse('2018-01-01'),
                'price' => '19.70',
                'thumbnail' => 'https://images.gr-assets.com/books/1526579445l/39947809.jpg'
            ),
            array(
                'title' => '2 States: Story of my marriage',
                'author' => 'Chetan Bhagat',
                'isbn' => '978006268145',
                'publisher' => 'HarperTeen',
                'published' => Carbon::parse('2018-01-01'),
                'price' => '19.70',
                'thumbnail' => 'https://i2.wp.com/writinggeeks.in/wp-content/uploads/2017/06/2-states-writing-geeks-1.jpg'
            ),
        );

        foreach ($books as $book){
            DB::table('books')->insert([
                'title' => $book['title'],
                'user_id'=>'1',
                'author' => $book['author'],
                'isbn'=> $book['isbn'],
                'publisher'=> $book['publisher'],
                'published'=> $book['published'],
                'price'=> $book['price'],
                'thumbnail' => $book['thumbnail']
            ]);
        }

        return factory(\App\Models\Book::class,10)->create();
    }
}
