<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Book::class, function (Faker $faker) {
    return [
        'user_id' => 1,
        'title' => $faker->sentence,
        'isbn' => $faker->unique()->isbn13,
        'author' => $faker->name,
        'publisher' => $faker->address,
        'published' => $faker->date(),
        'price' => $faker->numberBetween(1,100),
        'thumbnail' => $faker->imageUrl()
    ];
});
