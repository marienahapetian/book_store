<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    public function index()
    {
        $books = Book::paginate();

        return view('book.all',compact('books'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show( $id )
    {
        $book = Book::findOrFail($id);

        return view('book.single',compact('book'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create( )
    {
        return view('book.add');
    }

    /**
     * @param Request $request
     */
    public function store( Request $request )
    {
        $request->validate([
            'title' => 'required|max:255',
            'isbn' => 'required',
            'price' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'published' => 'required',
        ]);

        $book = new Book;
        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->price = $request->price;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->published = Carbon::parse( $request->published );
        $book->user_id = Auth::user()->id;

        try{
            $book->save();
        } catch (\Exception $e){
            return redirect()->back()->with('status', $e->getMessage());
        }

        return redirect()->back()->with('status', 'Book created successfully');

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit( $id )
    {
        $book = Book::findOrFail($id);

        return view('book.edit',compact('book'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update( $id , Request $request )
    {
        $request->validate([
            'title' => 'required|max:255',
            'isbn' => 'required',
            'price' => 'required',
            'author' => 'required',
            'publisher' => 'required',
            'published' => 'required',
        ]);

        $book = Book::findOrFail($id);

        $book->title = $request->title;
        $book->isbn = $request->isbn;
        $book->price = $request->price;
        $book->author = $request->author;
        $book->publisher = $request->publisher;
        $book->published = Carbon::parse( $request->published );
        $book->user_id = Auth::user()->id;

        $book->save();

        return view('book.single',compact('book'));
    }

    /**
     * @param $id
     */
    public function delete( $id )
    {
        $book = Book::findOrFail($id);

        $book->delete();

        return redirect()->back();
    }
}
