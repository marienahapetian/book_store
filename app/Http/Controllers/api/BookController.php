<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Http\Resources\Book as BookResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api');
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        // get books collection
        $books = Book::all();

        // return collection of books as resource
        return BookResource::collection($books);
    }


    /**
     * @param Book $book
     * @param Request $request
     * @return BookResource|\Illuminate\Http\JsonResponse
     */
    public function show(Book $book, Request $request)
    {
        if (auth()->guard('api')->user()->id !== $book->user_id)
            return response()->json(['error' => 'Unauthorised Action'], 401);

        return new BookResource($book);
    }

    /**
     * @param Book $book
     * @param Request $request
     * @return BookResource
     */
    public function update(Book $book, Request $request)
    {
        if (auth()->guard('api')->user()->id !== $book->user_id)
            return response()->json(['error' => 'Unauthorised Action'], 401);

        $book->title = $request->input('title') ?: $book->title;
        $book->author = $request->input('author') ?: $book->author;
        $book->publisher = $request->input('publisher') ?: $book->publisher;
        $book->published = $request->input('published') ? Carbon::parse($request->input('published')) : $book->published;
        $book->isbn = $request->input('isbn') ?: $book->isbn;
        $book->price = $request->input('price') ?: $book->price;

        if ($book->save())
            return new BookResource($book);
        else
            return response()->json(['error' => 'Book Could not be updated'], 401);
    }

    /**
     * @param Request $request
     * @return BookResource
     */
    public function store(Request $request)
    {
        try{
            $request->validate([
                'title' => 'required|max:255',
                'isbn' => 'required',
                'price' => 'required',
                'author' => 'required',
                'publisher' => 'required',
                'published' => 'required',
            ]);

            $book = new Book;
            $book->title = $request->input('title');
            $book->author = $request->input('author');
            $book->publisher = $request->input('publisher');
            $book->published = Carbon::parse($request->input('published'));
            $book->isbn = $request->input('isbn');
            $book->price = $request->input('price');
            $book->user_id = auth()->guard('api')->user()->id;

            if ($book->save())
                return new BookResource($book);
            else
                return response()->json(['error' => 'Book Could not be added'], 401);

        } catch (\Exception $e){
            return response()->json(['error' => $e->errors()], 401);
        }

    }

    /**
     * @param Book $book
     * @param Request $request
     * @return BookResource|\Illuminate\Http\JsonResponse
     */
    public function destroy(Book $book, Request $request)
    {
        if (auth()->guard('api')->user()->id !== $book->user_id)
            return response()->json(['error' => 'Unauthorised Action'], 401);

        if ($book->delete())
            return new BookResource($book);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $return_empty = false;

        foreach ($request->input() as $key => $value) {
            if (in_array($key, ['id', 'title', 'author', 'publisher', 'isbn', 'sortby', 'asc'])) {
                if ($value != '' && !in_array($key, ['sortby', 'asc'])) {
                    switch ($key) {
                        case 'title':
                        case 'author':
                        case 'publisher':
                            $books = isset($books) ? $books->where($key, 'LIKE', "%{$value}%") : Book::where($key, 'LIKE', "%{$value}%");
                            break;
                        case 'isbn':
                            $books = isset($books) ? $books->where($key, 'LIKE', "{$value}%") : Book::where($key, 'LIKE', "{$value}%");
                            break;
                        case 'id':
                            $books = isset($books) ? $books->where($key, $value) : Book::where($key, $value);
                            break;
                        default:
                            break;
                    }
                }
            } else {
                $return_empty = true;
                break;
            }
        }


        if ($return_empty || !isset($books)) {
            $books = collect(new Book);
        } else {
            if ($request->input('sortby') && $request->input('asc')) {
                $books->orderBy($request->input('sortby'), $request->input('asc'));
            } else if ($request->input('sortby') && !$request->input('asc')) {
                $books->orderBy($request->input('sortby'), 'desc');
            }

            $books = $books->get();
        }

        return BookResource::collection($books);

    }
}
