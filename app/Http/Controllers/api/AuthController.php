<?php

namespace App\Http\Controllers\api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Lcobucci\JWT\Parser;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function register( Request $request )
    {
        $request->validate([
            'email' => 'required | email',
            'name' => 'required',
            'password' => 'required',
        ]);

        $user = User::firstOrNew(['email'=>$request->email]);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt( $request->password );

        $user->save();

        $http = new Client;

        $response = $http->post( URL::to('/oauth/token' ), [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => '4Gm5XMsdQbpUIySaHgFksAGvDgb5muw5t4KcBNsp',
                'username' => $request -> email,
                'password' => $request -> password,
                'scope' => '*'
            ],
        ]);

        return json_decode((string) $response->getBody(), true);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function login( Request $request )
    {
        $request -> validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('email',$request->email)->first();

        if( ! $user ){
            return response('User does not exist',422 );
        } else {
            if( Hash::check( $request -> password, $user -> password)  ){
                $http = new Client;

                $response = $http->post( URL::to('/oauth/token' ), [
                    'form_params' => [
                        'grant_type' => 'password',
                        'client_id' => '2',
                        'client_secret' => '4Gm5XMsdQbpUIySaHgFksAGvDgb5muw5t4KcBNsp',
                        'username' => $request -> email,
                        'password' => $request -> password,
                        'scope' => '*'
                    ],
                ]);

                return json_decode((string) $response->getBody(), true);
            } else {
                return response('Password Mismatch',422 );
            }
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function logout( Request $request )
    {
        $value = $request->bearerToken();
        $id = (new Parser())->parse($value)->getHeader('jti');
        $token = auth()->guard('api')->user()->tokens->find($id);
        $token->revoke();

        $response = 'You have been successfully logged out';
        return response($response,200);
    }
}
