<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Book extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray( $request )
    {
        // returns all the data
        //return parent::toArray($request);

        return [
            'title' => $this->title,
            'isbn' => $this->isbn,
            'author' => $this->author,
            'publisher' => $this->publisher,
            'price' => $this->price,
        ];
    }

    public function with( $request )
    {
        return [
            //'version' => '1.0.0'
        ];
    }
}
