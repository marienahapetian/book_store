<?php

namespace Tests\Unit;

use App\Models\Book;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }


    /**
     * test if the length of isbn is 13
     * @test
     * @return void
     */
    public function test_isbn_start()
    {
        $isbn = Book::select('isbn')->first()->isbn;
        $this->assertStringStartsWith('97', $isbn);

    }
}
